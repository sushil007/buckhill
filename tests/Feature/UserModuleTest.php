<?php
namespace Tests\Unit;

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;

use Buckhill\Api\V1\Services\UserService;

class UserModuleTest extends TestCase {
    
    // public function test_userCreate() : void {
    //     $userService = new UserService();

    //     $userData = [
    //         'uuid'  => Str::uuid(),
    //         'first_name' => 'Test',
    //         'last_name' =>  'Test',
    //         'email' => 'test1@test.test',
    //         'password'  =>  Hash::make('admin'),
    //         'is_admin'  =>  1,
    //         'address'   =>  'test address',
    //         'phone_number'  => '123.123.123',
    //         'is_marketing'  => 0,
    //         'remember_token'    => Str::random(10)  
    //     ];
    //     $response = $userService->createAdminUser($userData);
    //     $this->assertInstanceOf(\Buckhill\Models\User::class, $response);
    // }

    public function test_AdminLogin() : void {
        $response = $this->postJson('/api/v1/admin/login', ['email'=>'admin@buckhill.co.uk', 'password'=>'admin']);
        $response->assertStatus(200)
                ->assertJson(fn(AssertableJson $json) => $json->has('token'));
    }

    public function test_UsersListing() : void {
        $response = $this->postJson('/api/v1/admin/login', ['email'=>'admin@buckhill.co.uk', 'password'=>'admin']);
        $response->assertStatus(200)
                ->assertJson(fn(AssertableJson $json) => $json->has('token'));
        
        $token = $response->original['token'];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])->getJson('/api/v1/admin/user-listing');
        
        $response->assertStatus(200)
                ->assertJson(fn(AssertableJson $json) => 
                        $json->hasAll('data', 'meta', 'links')
                            ->has('data.0', fn ($json) =>
                                            $json->has('uuid')
                                                ->has('first_name')
                                                ->has('last_name')
                                                ->missing('password')
                                                ->etc()
                        )
        );
    }
}