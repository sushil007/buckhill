<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Buckhill\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::factory()
                ->count(50)
                ->create();
        
        // create admin user manually
        DB::table('users')->insert([
            'uuid' => Str::uuid(),
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'is_admin' => 1,
            'email' => 'admin@buckhill.co.uk',
            'password' => Hash::make('admin'),
            'address' => 'Some address',
            'phone_number' => '123456789',
            'is_marketing' => 0,
            'email_verified_at' => now(),
            'remember_token' => Str::random(),
            'created_at' => now(),
        ]);        
    }
}
