<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'Buckhill\Api\V1\Controllers\WelcomeController@index');

Route::prefix('admin/')->group(function(Router $route) {
    Route::post('/login', 'Buckhill\Api\V1\Controllers\AdminController@adminLogin')->name('adminLogin');

    Route::group(['middleware'=>'VerifyAdminKey'], function(Router $route) {
        Route::get('/user-listing', 'Buckhill\Api\V1\Controllers\AdminController@userListing')->name('userListing');
    });
});
