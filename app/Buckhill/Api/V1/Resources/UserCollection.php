<?php
namespace Buckhill\Api\V1\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     *@OA\Schema(
     *  schema="UserCollection",
     *  @OA\Property(
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/User") 
     *  ),
     * )
    */
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

}