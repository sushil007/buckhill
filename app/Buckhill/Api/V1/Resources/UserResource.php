<?php
namespace Buckhill\Api\V1\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     *@OA\Schema(
     *  schema="User",
     *  @OA\Property(
     *     property="uuid",
     *     type="integer",
     *     description="Primary Key"
     *  ),
     *  @OA\Property(
     *     property="first_name",
     *     type="string",
     *     description="first name"
     *  ),
     *  @OA\Property(
     *     property="last_name",
     *     type="string",
     *     description="last name"
     *  ),
     *  @OA\Property(
     *     property="email",
     *     type="string",
     *     description="email address"
     *  ),
     *  @OA\Property(
     *     property="email_verified_at",
     *     type="string",
     *     description="date time string on which the email was verified"
     *  ),
     *  @OA\Property(
     *     property="avatar",
     *     type="string",
     *     description="avatar"
     *  ),
     *  @OA\Property(
     *     property="is_marketing",
     *     type="string",
     *     description="is marketing"
     *  ),
     *  @OA\Property(
     *     property="address",
     *     type="string",
     *     description="address"
     *  ),
     *  @OA\Property(
     *     property="phone_number",
     *     type="string",
     *     description="phone_number"
     *  ),
     *  @OA\Property(
     *     property="created_at",
     *     type="string",
     *     description="created at"
     *  ),
     *  @OA\Property(
     *     property="updated_at",
     *     type="string",
     *     description="updated at"
     *  ),
     *  @OA\Property(
     *     property="last_login_at",
     *     type="string",
     *     description="last login at"
     *  ),
     * )
    */
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid'=>$this->uuid,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'email'=>$this->email,
            'email_verified_at'=>$this->email_verified_at,
            'avatar'=>$this->avatar,
            'address'=> $this->address,
            'phone_number' => $this->phone_number,
            'is_marketing' => $this->is_marketing,
            'created_at' => $this->created_at,
            'updated_at' =>  $this->updated_at,
            'last_login_at' => $this->last_login_at
        ];
    }

}