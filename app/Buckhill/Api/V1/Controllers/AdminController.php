<?php
namespace Buckhill\Api\V1\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Buckhill\Api\V1\Services\JWTService;
use Buckhill\Api\V1\Services\UserService;

use Buckhill\Models\User;

use Buckhill\Api\V1\Resources\UserResource;
use Buckhill\Api\V1\Resources\UserCollection;

use Buckhill\Api\V1\Controllers\ApiController;

class AdminController extends ApiController
{
    
    /**
     * @var UserService;
     */
    public $service;
    /**
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @OA\Post(
     *      path="/admin/login",
     *      operationId="AdminLogin"
     *      tags={"Admin"},
     *      summary="Admin user login",
     * 
     *      @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Returns  authorization token",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Validation errors",
     *          @OA\Schema(
     *              type="array",
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      )
     * )
     */
    /**
     * @param Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminLogin(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'email' =>  'email|required',
            'password'  =>  'required'
            ]
        );

        if($validator->fails()) {
            return $this->respondError(422, $validator->errors());
        }
        $validated = $validator->validated();
        $email = $validated['email'];
        $password = $validated['password'];
        $is_admin = 1;

        $result = Auth::attempt(compact('email', 'password', 'is_admin'), true);
        if (!$result) {
            return $this->respondError(401, ['error'=>'Unauthorized']);
        }

        // generate JWT token
        return $this->generateToken(Auth::user());
    }

    
    /**
     * @OA\Get(
     *      path="/admin/user-listing",
     *      operationId="AdminUserListing"
     *      tags={"Admin"},
     *      summary="User Listing",
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="order",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="first_name",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="phone",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="address",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Returns list of users",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#components/schemas/UserCollection"),
     *          ),
     *      ),
     * )
     */
    /**
     * @param Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function userListing(Request $request)
    {
        $data = $request->all();
        $limit = $data['limit'] ?? 20;
        $sortBy = $data['sortBy'] ?? 'id';
        $order = $data['order'] ?? 'desc';
        $firstName = $data['first_name'] ?? '';
        $email = $data['email'] ?? '';
        $phone = $data['phone'] ?? '';
        $address = $data['address'] ?? '';

        return $this->respondSuccess(new UserCollection($this->service->getUsers($limit, $sortBy, $order, $firstName, $email, $phone, $address)));
    }

    /**
     * @param \Buckhill\Models\User $user
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function generateToken(User $user)
    {
        $jwtService = new JWTService();
        $jwtService->issueToken($user->uuid);
        $token = $jwtService->getToken();
        return response()->json(['token'=>$token]);
    }
}