<?php
namespace Buckhill\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ApiController extends Controller
{

    /**
     * @param mixed $data
     * @param int   $status
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondSuccess(mixed $data = [], int $status = 200)
    {
        if($data instanceof ResourceCollection) {
            return $data;
        } else {
            return response()->json(['data'=>$data], $status);
        }
    }

    /**
     * @param int   $status
     * @param mixed $error
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondError(int $status = 403, mixed $error = [])
    {
        return response()->json($error, $status);
    }
}