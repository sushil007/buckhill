<?php
namespace Buckhill\Api\V1\Controllers;

use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    /**
     * @OA\Info(
     *      title="Buckhill API Documentation",
     *      version="1.0.0"
     * )
     * 
     * @OA\PathItem(
     *      path="/"
     * )
     * 
     * @OA\Server(
     *      description="Buckhill API",
     *      url="https://localhost/api/v1"
     * )
     * 
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     * )
     */     
    public function index()
    {
        $response = [
            'success' => true,
            'message' => 'Welcome to API v1'
        ];

        return response()->json($response);
    }
}