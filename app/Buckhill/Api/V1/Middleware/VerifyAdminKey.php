<?php
namespace Buckhill\Api\V1\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;


use Buckhill\Api\V1\Services\JWTService;
/**
 * Middleware to verify Authorization token
 */
class VerifyAdminKey extends Middleware
{
    /**
     * Handle incoming request
     *
     * @param mixed   $request
     * @param Closure $next
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if($request->header('Authorization')) {
            $token = $request->header('Authorization');
            
            $token = trim(str_replace('Bearer', '', $token));

            if(!(new JWTService())->validateToken($token)) {
                return response()->json(['error'=>'Invalid authorization token']);
            }

            return $next($request);

        }
        
        return response()->json(['error'=>'Authorization token missing'], 403);
    }
}