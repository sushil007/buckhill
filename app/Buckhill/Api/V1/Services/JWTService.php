<?php
namespace Buckhill\Api\V1\Services;

use \DateTimeImmutable;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\UnencryptedToken;
use Lcobucci\JWT\Validation\RequiredConstraintsViolated;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;

use Illuminate\Support\Str;

/**
 * [Description JWTService]
 */
class JWTService
{
    /**
     * @var \Lcobucci\JWT\Configuration
     */
    public $configuration;
    
    public $token;

    /**
     */
    public function __construct()
    {
        $privateKey = InMemory::file(__DIR__.'/../../../../../private.key');
        $publicKey = InMemory::file(__DIR__.'/../../../../../public.pub');

        $this->configuration = Configuration::forAsymmetricSigner(new Signer\Rsa\Sha256(), $privateKey, $publicKey);

        $this->configuration->setValidationConstraints(new IssuedBy(url('/')));
    }

    /**
     * Issues token based upon uuid
     *
     * @param mixed $uuid
     * 
     * @return void
     */
    public function issueToken($uuid) : void
    {
        $now = new DateTimeImmutable();
        $this->token = $this->configuration->builder()
            ->issuedBy(url('/'))
            ->identifiedBy(Str::uuid())
            ->issuedAt($now)
            ->withClaim('uid', $uuid)
            ->getToken($this->configuration->signer(), $this->configuration->signingKey());
    }

    
    /**
     * @return String
     */
    public function getToken() : String
    {
        return $this->token->toString();
    }

    /**
     * Validates token
     *
     * @param string $token
     * 
     * @return bool
     */
    public function validateToken(string $token) : bool
    {
        $token = $this->configuration->parser()->parse($token);
        assert($token instanceof UnencryptedToken);
        
        $constraints = $this->configuration->validationConstraints();

        if(!$this->configuration->validator()->validate($token, ...$constraints)) {
            return false;
        }
        return true;
    }
}