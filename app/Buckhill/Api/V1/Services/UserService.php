<?php
namespace Buckhill\Api\V1\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use Buckhill\Models\User;

/**
 * actions related to Users
 */
class UserService
{

    /**
     * @param Array $userData
     * 
     * @return User
     */
    public function createAdminUser(Array $userData): User
    {
        return  User::create($userData);
    }

    /**
     * @param int    $limit
     * @param string $sortBy
     * @param string $order
     * 
     * @return LengthAwarePaginator
     */
    public function getUsers(int $limit, string $sortBy, string $order, ?string $firstName = '', ?string $email='', ?string $phone = '', ?string $address='') : LengthAwarePaginator
    {
        $query = User::query();
        
        if($firstName != '') {
            $query->where('first_name', 'like', '%'.$firstName.'%');
        }
        if($email != '') {
            $query->where('email', 'like', '%'.$email.'%');
        }
        if($phone != '') {
            $query->where('phone_number', 'like', '%'.$phone.'%');
        }
        if($address != '') {
            $query->where('address', 'like', '%'.$address.'%');
        }

        return $query->orderBy($sortBy, $order)->paginate($limit);
    }
}